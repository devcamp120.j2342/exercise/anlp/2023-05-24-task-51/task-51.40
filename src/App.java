import java.util.ArrayList;
import java.util.Collections;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Subtask 1");
        ArrayList<Integer> numbers1 = new ArrayList<>();
        // Thêm 10 số nguyên vào danh sách
        numbers1.add(5);
        numbers1.add(10);
        numbers1.add(2);
        numbers1.add(8);
        numbers1.add(3);
        numbers1.add(7);
        numbers1.add(1);
        numbers1.add(6);
        numbers1.add(4);
        numbers1.add(9);
        System.out.println("Danh sách ban đầu: " + numbers1);
        // Sắp xếp danh sách theo thứ tự giá trị tăng dần
        Collections.sort(numbers1);
        System.out.println("Danh sách sau khi sắp xếp: " + numbers1);

        System.out.println("Subtask 2");
        ArrayList<Integer> numbers2 = new ArrayList<>();
        numbers2.add(5);
        numbers2.add(10);
        numbers2.add(2);
        numbers2.add(8);
        numbers2.add(3);
        numbers2.add(7);
        numbers2.add(1);
        numbers2.add(6);
        numbers2.add(4);
        numbers2.add(9);

        // In ra ArrayList vừa tạo
        System.out.println("ArrayList vừa tạo: " + numbers2);

        // Tạo mới ArrayList Integer mới từ các số có giá trị từ 10 đến 100
        ArrayList<Integer> newNumbers2 = new ArrayList<>();
        for (int i = 10; i <= 100; i++) {
            newNumbers2.add(i);
        }

        System.out.println("Subtask 3");
        // In ra ArrayList mới chứa các số từ 10 đến 100
        System.out.println("ArrayList mới: " + newNumbers2);

        // Tạo mới ArrayList String và thêm 5 màu sắc
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Yellow");
        colors.add("Orange");

        // In ra ArrayList vừa tạo
        System.out.println("ArrayList màu sắc: " + colors);

        // Kiểm tra nếu ArrayList chứa màu vàng
        if (colors.contains("Yellow")) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }


        System.out.println("Subtask 4");

        // Tạo mới ArrayList Integer và thêm 10 số
        ArrayList<Integer> numbers4 = new ArrayList<>();
        numbers4.add(5);
        numbers4.add(10);
        numbers4.add(2);
        numbers4.add(8);
        numbers4.add(3);
        numbers4.add(7);
        numbers4.add(1);
        numbers4.add(6);
        numbers4.add(4);
        numbers4.add(9);

        // Tính tổng các số trong ArrayList
        int sum = 0;
        for (int number : numbers4) {
            sum += number;
        }

        // In ra tổng các số
        System.out.println("Tổng các số trong ArrayList: " + sum);


        System.out.println("Subtask 5");

        // Tạo mới ArrayList String và thêm 5 màu sắc
        ArrayList<String> colors2 = new ArrayList<>();
        colors2.add("Red");
        colors2.add("Green");
        colors2.add("Blue");
        colors2.add("Yellow");
        colors2.add("Orange");

        // Xóa bỏ toàn bộ giá trị của ArrayList
        colors2.clear();

        // In ra giá trị mới của ArrayList
        System.out.println("ArrayList sau khi xóa: " + colors2);
    
    }
}
